﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Soundboard
{
    class SoundResources
    {
        private static Dictionary<string, string> soundFiles = null;
        private static Dictionary<string, SoundClipMetaData> soundMetaData = null;

        private static void initializeSoundResourcesIfNeeded()
        {
            if (soundFiles == null || soundMetaData == null)
            {
                initialSoundResources();
            }
        }

        private static void initialSoundResources()
        {
            soundFiles = new Dictionary<string, string>();
            soundMetaData = new Dictionary<string, SoundClipMetaData>();

            foreach (string soundFilePath in getSoundFilePaths())
            {
                string soundFileName = Path.GetFileName(soundFilePath);

                soundFiles.Add(soundFileName, soundFilePath);
                soundMetaData.Add(soundFileName, SoundClipMetaDataProcessor.getTag(soundFilePath));
            }
        }

        private static string[] getSoundFilePaths()
        {
            string fullSoundFolderPath = AppDomain.CurrentDomain.BaseDirectory + Configuration.getSoundsFolder();
            return Directory.GetFiles(fullSoundFolderPath);
        }

        public static string[] getSoundNames()
        {
            initializeSoundResourcesIfNeeded();

            return soundFiles.Keys.ToArray();
        }

        public static String getSoundFilePath(string soundName)
        {
            initializeSoundResourcesIfNeeded();

            return soundFiles[soundName];
        }

        public static SoundClipMetaData getMetaData(string soundName)
        {
            initializeSoundResourcesIfNeeded();

            return soundMetaData[soundName];
        }
    }
}
