﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Soundboard
{
    public class SoundClipMetaData
    {
        public String Title { get; private set; }
        public String Artist { get; private set; }
        public String Comment { get; private set; }

        public SoundClipMetaData(String title, String artist, String comment)
        {
            Title = title;
            Artist = artist;
            Comment = comment;
        }
    }

    class SoundClipMetaDataProcessor
    {
        public static SoundClipMetaData getTag(string soundFilePath)
        {
            string stringifiedMusicFile;
            try
            {
                stringifiedMusicFile = stringifySoundFile(soundFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Soundfile not found... or something. Real exception: " + e.ToString());
                return new SoundClipMetaData(Path.GetFileName(soundFilePath), "", "");
            }

            String title = getWrappedText(stringifiedMusicFile, "1");
            String artist = getWrappedText(stringifiedMusicFile, "2");
            String comment = getWrappedText(stringifiedMusicFile, "3");

            // we can't have a blank title, so just use the file name
            if (String.IsNullOrEmpty(title))
            {
                title = Path.GetFileName(soundFilePath);
            }

            return new SoundClipMetaData(title, artist, comment);
        }

        private static string stringifySoundFile(string soundFilePath)
        {
            using (FileStream fs = File.OpenRead(soundFilePath))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                return Encoding.UTF8.GetString(buffer);
            }
        }

        private static string getWrappedText(string text, string wrapperSymbol)
        {
            string fullWrapper = @"\|" + wrapperSymbol;
            Regex regex = new Regex(fullWrapper + @".*" + fullWrapper);
            string wrappedString = regex.Match(text).ToString();
            return wrappedString.Replace("|" + wrapperSymbol, "");
        }
    }
}
