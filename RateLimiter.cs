﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Soundboard
{
    class RateLimiter
    {
        public RateLimiter(int cooldown)
        {
            RATE_LIMIT_DELAY_IN_SECONDS = cooldown;
        }

        private Dictionary<string, Int32> timestamps = new Dictionary<string, Int32>();
        private int RATE_LIMIT_DELAY_IN_SECONDS;

        public Boolean canUserPlaySound(string ipAddress)
        {
            return getTimeLeft(ipAddress) <= 0;
        }

        public void rateLimitIPAddress(string ipAddress)
        {
            timestamps.Remove(ipAddress);
            timestamps.Add(ipAddress, getUnixTimestamp());
        }

        public Int32 getTimeLeft(string ipAddress)
        {
            Int32 timeUserLastPlayedSound = getLastPlayedTime(ipAddress);
            Int32 now = getUnixTimestamp();

            return (timeUserLastPlayedSound + RATE_LIMIT_DELAY_IN_SECONDS) - now;
        }

        private Int32 getLastPlayedTime(string ipAddress)
        {
            if (!timestamps.ContainsKey(ipAddress))
            {
                return 0;
            }

            return timestamps[ipAddress];
        }

        private Int32 getUnixTimestamp()
        {
            return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}
