﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Soundboard
{
    class SoundboardController
    {
        public SoundboardController()
        {
            rateLimiter = new RateLimiter(Configuration.getBaseCooldown());
        }

        private RateLimiter rateLimiter;

        public void playSound(string soundName, string ipAddress)
        {
            if (!rateLimiter.canUserPlaySound(ipAddress))
            {
                return;
            }

            Console.WriteLine(DateTime.Now + " --- " + ipAddress + " --- " + soundName);

            rateLimiter.rateLimitIPAddress(ipAddress);

            System.Media.SoundPlayer player = new System.Media.SoundPlayer(SoundResources.getSoundFilePath(soundName));
            player.PlaySync();
        }

        public Int32 getTimeLeft(string ipAddress)
        {
            return rateLimiter.getTimeLeft(ipAddress);
        }

        public Int32 getCooldownTime()
        {
            return Configuration.getBaseCooldown();
        }
    }
}
