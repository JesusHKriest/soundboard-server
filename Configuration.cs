﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Soundboard
{
    class Configuration
    {
        private static String configurationDirectory = AppDomain.CurrentDomain.BaseDirectory + @"\";

        private static String getConfigurationValue(String fileName)
        {
            return File.ReadAllText(configurationDirectory + fileName + ".txt");
        }

        private static int baseCooldown = int.Parse(getConfigurationValue(@"cooldown"));

        public static int getBaseCooldown()
        {
            return baseCooldown;
        }

        private static String soundsFolder = getConfigurationValue(@"folder");

        public static String getSoundsFolder()
        {
            return soundsFolder;
        }
    }
}
