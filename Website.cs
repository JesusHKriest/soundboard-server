﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using System.IO;
using System.Reflection;
using System.Net.Sockets;

namespace Soundboard
{
    public class MyHttpServer : HttpServer
    {
        private SoundboardController soundboardController = new SoundboardController();

        public MyHttpServer(int port) : base(port)
        {
        }

        public override void handleGETRequest(HttpProcessor p)
        {
            String url = p.getURL();

            if (url.Equals("/favicon.ico"))
            {
                writeFavIcon(p);
            }
            else if (url.Contains("/" + Configuration.getSoundsFolder() + "/"))
            {
                writePreviewSound(p, url);
            }
            else
            {
                writeIndex(p);
            }
        }

        private void writeFavIcon(HttpProcessor p)
        {
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            string iconPath = applicationPath + "/icon.ico";
            byte[] bytes = File.ReadAllBytes(iconPath);
            p.outputStream.BaseStream.Write(bytes, 0, bytes.Length);
            p.writeSuccess("image/x-icon");
        }

        private void writePreviewSound(HttpProcessor p, String url)
        {
            url = Uri.UnescapeDataString(url);

            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            string soundPath = applicationPath + url;
            byte[] bytes = File.ReadAllBytes(soundPath);
            p.outputStream.BaseStream.Write(bytes, 0, bytes.Length);
            p.writeSuccess("audio/wav");
        }

        private void writeIndex(HttpProcessor p)
        {
            Int32 rateLimitForIPAddress = soundboardController.getTimeLeft(getIPAddress(p));

            p.writeSuccess();
            p.outputStream.WriteLine("<html><body><h1>Soundboard Server</h1>");

            int secondsLeft = soundboardController.getTimeLeft(getIPAddress(p));
            if (secondsLeft > 0)
            {
                p.outputStream.WriteLine("<span id=\"timer\">You can play a sound again in " + secondsLeft + " seconds.</span><br/><br/>");
            }
            else
            {
                p.outputStream.WriteLine("<span id=\"timer\">You can play a sound again now.</span><br/><br/>");
            }

            p.outputStream.WriteLine("<div style=\"height:84%;overflow:auto;\">");
            p.outputStream.WriteLine("<table style=\"width:100%\" border=\"1\" text-align=\"center\">");

            p.outputStream.WriteLine("<tr>");
            p.outputStream.WriteLine("<td><b>Play sound</b></td>");
            p.outputStream.WriteLine("<td><b>Title</b></td>");
            p.outputStream.WriteLine("<td><b>Speaker</b></td>");
            p.outputStream.WriteLine("<td><b>Description</b></td>");
            p.outputStream.WriteLine("<td><b>Preview</b></td>");
            p.outputStream.WriteLine("</tr>");

            foreach (string fileName in SoundResources.getSoundNames())
            {
                addSoundButtonHTML(p, fileName);
            }
            p.outputStream.WriteLine("</table>");
            p.outputStream.WriteLine("</div>");

            addCountdownTimerJavascript(p, soundboardController.getTimeLeft(getIPAddress(p)));
            p.outputStream.WriteLine("<p align=\"right\">&#169; JesusHKriest 2016</p>");
            p.outputStream.WriteLine("</html>");
        }

        private void addSoundButtonHTML(HttpProcessor p, string soundName)
        {
            SoundClipMetaData metaData = SoundResources.getMetaData(soundName);
            p.outputStream.WriteLine("<tr>");
            p.outputStream.WriteLine("<td><button onclick=\"onClick(\'" + soundName + "\')\">Play</button></td>");
            p.outputStream.WriteLine("<td>" + metaData.Title + "</td>");
            p.outputStream.WriteLine("<td>" + metaData.Artist + "</td>");
            p.outputStream.WriteLine("<td>" + metaData.Comment + "</td>");
            p.outputStream.WriteLine("<td><audio controls=\"controls\" preload=\"none\"> <source src=\"/" + Configuration.getSoundsFolder() + "/" + soundName + "\" type=\"audio/wav\">Your browser does not support audio playback.</audio></td>");
            p.outputStream.WriteLine("</tr>");
        }

        private void addCountdownTimerJavascript(HttpProcessor p, Int32 secondsLeft)
        {
            p.outputStream.WriteLine("<SCRIPT LANGUAGE=\"JAVASCRIPT\" TYPE=\"TEXT/JAVASCRIPT\">");
            p.outputStream.WriteLine("var count=" + secondsLeft + ";");
            p.outputStream.WriteLine("var counter=setInterval(timer, 1000);");
            p.outputStream.WriteLine("function timer()");
            p.outputStream.WriteLine("{");
                p.outputStream.WriteLine("count=count-1;");
                p.outputStream.WriteLine("document.getElementById(\"timer\").innerHTML=\"You can play a sound again in \" + count + \" seconds.\";");
                p.outputStream.WriteLine("if (count <= 0)");
                p.outputStream.WriteLine("{");
                    p.outputStream.WriteLine("document.getElementById(\"timer\").innerHTML=\"You can play a sound again now.\";");
                    p.outputStream.WriteLine("return;");
                p.outputStream.WriteLine("}");
            p.outputStream.WriteLine("}");
            p.outputStream.WriteLine("function onClick(soundName)");
            p.outputStream.WriteLine("{");
                p.outputStream.WriteLine("var xhttp = new XMLHttpRequest();");
                p.outputStream.WriteLine("var baseUrl = \"/playSound?sound=\";");
                p.outputStream.WriteLine("baseUrl += soundName;");
                p.outputStream.WriteLine("xhttp.onreadystatechange = function () {");
                p.outputStream.WriteLine("if (xhttp.readyState == 4 && xhttp.status == 200)");
                p.outputStream.WriteLine("{");
                    p.outputStream.WriteLine("console.log(xhttp.responseText);");
                p.outputStream.WriteLine("}");
            p.outputStream.WriteLine("};");
            p.outputStream.WriteLine("xhttp.open(\"post\", baseUrl, true);");
            p.outputStream.WriteLine("xhttp.setRequestHeader(\"Content-type\",\"application/x-www-form-urlencoded\");");
            p.outputStream.WriteLine("xhttp.send();");
            p.outputStream.WriteLine("if (!(count > 0))");
            p.outputStream.WriteLine("{");
                p.outputStream.WriteLine("count = " + soundboardController.getCooldownTime());
            p.outputStream.WriteLine("}");
            p.outputStream.WriteLine("}");
            p.outputStream.WriteLine("</SCRIPT>");
        }

        public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData)
        {
            if (getRequestType(p).Equals("/playSound"))
            {
                handlePlaySoundRequest(p);
            }
        }

        private String getRequestType(HttpProcessor p)
        {
            string URL = p.http_url;
            int indexOfQuestionMark = URL.IndexOf("?");
            string requestType = URL.Substring(0, indexOfQuestionMark);
            return requestType;
        }

        private void handlePlaySoundRequest(HttpProcessor p)
        {
            try
            {
                soundboardController.playSound(getSoundName(p), getIPAddress(p));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private string getSoundName(HttpProcessor p)
        {
            string URL = p.http_url;
            int indexOfEqualsSign = URL.IndexOf("=") + 1;
            string unescapedSoundName = URL.Substring(indexOfEqualsSign, URL.Length - indexOfEqualsSign);
            string escapedSoundName = Uri.UnescapeDataString(unescapedSoundName);
            return escapedSoundName;
        }

        private string getIPAddress(HttpProcessor p)
        {
            TcpClient socket = p.socket;
            Socket socket2 = socket.Client;
            string portedIPAddress = socket2.RemoteEndPoint.ToString();
            int indexOfPortNumber = portedIPAddress.IndexOf(":");
            string unportedIPAddress = portedIPAddress.Substring(0, indexOfPortNumber);
            return unportedIPAddress;
        }
    }
}
